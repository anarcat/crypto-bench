Crypto tokens benchmarking suite
================================

This script will test decryption performance of GPG, particularly with
smart cards. Note that it will wipe the encryption key of the smart
card you provide it, so it's a dangerous thing to do on a real card.

Test data can be generated automatically by the script, or by hand,
using the directives documented below.

Once CSV data is generated, graphs can be displayed with the script
without having to rerun the test suite. The script expects a CSV file
with 3 columns: device, algorithm and time. The first two are strings,
the latter is a float, in seconds. Note that we count the total
runtime of GPG, which yields lower bandwidth results than what the
card is probably capable of doing. We discount that error on the
grounds that it affects all tests uniformly.

Provisionnal results
--------------------

![Decryption speed (s) results for 16b packets](results-16b.png)

The above graph show the media time in seconds to decrypt a 16 byte
file using various security tokens. The error bars show the standard
deviation of the 1000 samples extracted. Columns are ordered by the
fastest RSA-2048 calculation and was finally generated with:

    ./benchtokens.py results-16b.csv -k CPU Yubikey-4 Nitrokey-PRO Yubikey-NEO FST-01 -o results-16b.png

Raw data is available in [this CSV file](results-16b.csv). Actual
numbers are visible in the graphs, in seconds, or below:

    In [1]: data = pandas.read_csv('results-16b.csv')
    
    In [2]: data.groupby(['algorithm', 'device'])['time'].describe()
    Out[2]: 
    algorithm        device             
    ECDH-Curve25519  CPU           count    100.000000
                                   mean       0.036028
                                   std        0.000304
                                   min        0.035348
                                   25%        0.035811
                                   50%        0.036034
                                   75%        0.036180
                                   max        0.036745
                     FST-01        count    100.000000
                                   mean       0.135268
                                   std        0.004923
                                   min        0.108354
                                   25%        0.135048
                                   50%        0.136223
                                   75%        0.137005
                                   max        0.142429
    RSA-2048         CPU           count    100.000000
                                   mean       0.015521
                                   std        0.001147
                                   min        0.014833
                                   25%        0.015219
                                   50%        0.015408
                                   75%        0.015550
                                   max        0.025842
                     FST-01        count    100.000000
                                   mean       1.265058
                                   std        0.006684
                                   min        1.257821
                                   25%        1.260325
                                   50%        1.261329
                                               ...    
    RSA-4096         CPU           std        0.000568
                                   min        0.042437
                                   25%        0.042842
                                   50%        0.043039
                                   75%        0.043196
                                   max        0.047615
                     FST-01        count    100.000000
                                   mean       8.217803
                                   std        0.006450
                                   min        8.209027
                                   25%        8.213739
                                   50%        8.214600
                                   75%        8.221799
                                   max        8.238128
                     Nitrokey-PRO  count    100.000000
                                   mean       3.149798
                                   std        0.016689
                                   min        3.085639
                                   25%        3.141080
                                   50%        3.150280
                                   75%        3.158308
                                   max        3.183072
                     Yubikey-4     count    100.000000
                                   mean       0.875137
                                   std        0.009845
                                   min        0.856780
                                   25%        0.868401
                                   50%        0.873192
                                   75%        0.881244
                                   max        0.915362
    Name: time, dtype: float64


Reproducing results automatically
---------------------------------

The above results were produced (more or less) with the following command:

    ./bench-tokens.py -v --save results-16b.csv -o results-16b.png \
         --keycard CPU Yubikey-4 Yubikey-NEO FST-01 Nitrokey-PRO \
         --algorithm RSA-2048 RSA-4096 EDDSA-Ed25519 \
         --iterations 100 --size 16

In reality, the results were produced in multiple stages, one key at a
time, and sometimes one algorithm at a time. For example, only the
FST-01 supporst ECC algorithms so a separate run was done for
this. Furthermore, the Nitrokey results were produced about 2 weeks
after the other results, using 3 seperate batches because of problems
with the script, using commandlines like:

    ./bench-tokens.py -v --save results-16b-Nitrokey-PRO-RSA-2048.csv --keycard Nitrokey-PRO
    ./bench-tokens.py -v --save results-16b-Nitrokey-PRO-RSA-4096.csv -a RSA-4096 --keycard Nitrokey-PRO

And results were unified using:

    ( cat results-16b.csv; tail -n +2 results-16b-Nitrokey-PRO-RSA-2048.csv results-16b-Nitrokey-PRO-RSA-4096.csv ) | sponge results-16b.csv

Also, default parameters were used instead of specifying them on the
commandline; they are only explicitly mentioned here to make sure
there is not confusion in the future.

Reproducing results by hand
---------------------------

To generate the data by hand, use the following procedure:

  1. very important: work in a temporary keyring:

        export GNUPGHOME=$(mktemp -d)

  1. create a 100M file of garbage

        dd if=/dev/urandom of=testfile bs=1k count=100k

  2. generate a new private keypair:

```
gpg --batch --generate-key <<EOF
%transient-key
%no-protection
Key-Type: RSA
Key-Length: 2048
Key-Usage: sign
Subkey-Type: RSA
Subkey-Length: 2048
Subkey-Usage: encrypt
Name-Real: test key
Name-Email: test@example.com
Expire-Date: 1w
EOF
```

  3. encrypt stuff to the key:

        gpg --encrypt -r test@example.com testfile

  4. benchmark decryption, 101 times (first will prompt for password
     so not relevant):

        $ time gpg --quiet --decrypt < testfile.gpg | pv > /dev/null
        1.08user 0.06system 0:01.16elapsed 98%CPU (0avgtext+0avgdata 5072maxresident)k
        0inputs+0outputs (0major+265minor)pagefaults 0swaps
         100MiO 0:00:01 [85,5MiB/s] [     <=>                   ]
        $ for i in $(seq 100) ; do
               time gpg --quiet --decrypt < testfile.gpg 2>> i3-6100.txt | pv > /dev/null
        done

  5. move key to card:

        gpg --edit-key test@example.com
        key 1
        keytocard
        save

  6. repeat step 4 with the Yubikey NEO:

        $ time gpg --quiet --decrypt < testfile.gpg | pv > /dev/null
        1.12user 0.04system 0:06.76elapsed 17%CPU (0avgtext+0avgdata 4952maxresident)k
        0inputs+0outputs (0major+264minor)pagefaults 0swaps
         100MiO 0:00:06 [14,8MiB/s] [     <=>                 ]
        $ for i in $(seq 100) ; do
              time gpg --quiet --decrypt < testfile.gpg 2>> Yubikey-NEO.txt | pv > /dev/null
         done
         100MiO 0:00:01 [53,1MiB/s] [     <=>                 ]
        [...]

  7. repeat step 2-6 with the FST-01:

        $ time gpg --quiet --decrypt < testfile.gpg | pv > /dev/null
        1.14user 0.03system 0:07.03elapsed 16%CPU (0avgtext+0avgdata 4752maxresident)k
        0inputs+0outputs (0major+258minor)pagefaults 0swaps
         100MiO 0:00:07 [14,2MiB/s] [        <=>             ]
        $ for i in $(seq 100) ; do
              time gpg --quiet --decrypt < testfile.gpg 2>> FST-01.txt | pv > /dev/null
          done
         100MiO 0:00:02 [41,3MiB/s] [     <=>                ]
        [...]

  8. repeat step 2-6 with the Yubikey 4:

        $ time gpg --quiet --decrypt < testfile.gpg | pv > /dev/null
        1.14user 0.04system 0:03.48elapsed 33%CPU (0avgtext+0avgdata 4852maxresident)k
        0inputs+0outputs (0major+260minor)pagefaults 0swaps
         100MiO 0:00:03 [28,7MiB/s] [     <=>                ]
        $ for i in $(seq 100) ; do
              time gpg --quiet --decrypt < testfile.gpg 2>> Yubikey-4.txt | pv > /dev/null
          done
         100MiO 0:00:01 [76,1MiB/s] [     <=>                ]
        [...]

  8. extract elapsed time:

        for f in *.txt ; do
            sed -n "/elapsed/{s/^.* 0:\([0-9]*.[0-9]*\)elapsed.*\$/rsa2048,$(basename $f .txt),\1/;p}" "$f"
        done > results-2048.csv

  9. repeat steps 2-7 with 4096 bit keys

  10. repeat steps 2-7 with ECC keys:
  
```
gpg --batch --expert --generate-key <<EOF
%transient-key
%no-protection
Key-Type: EDDSA
Key-Curve: Ed25519
Key-Usage: sign
Subkey-Curve: ECDH
Subkey-Length: Curve25519
Subkey-Usage: encrypt
Name-Real: test key
Name-Email: test@example.com
Expire-Date: 1w
EOF
```

  10. merge into the CSV file:

        ( echo 'algorithm,device,time' ; cat results-*.csv ) > results.csv

  11. generate the graph:

        python histo.py results.csv --output results.png

Similar softare
---------------

Another tool called [graphene-cli][] is aimed at testing PKCS#11
interfaces has a lower overhead as it doesn't use GnuPG. It was
discovered late in the writing of this software and seems
much [harder][] to [use][].

[use]: https://github.com/PeculiarVentures/graphene-cli/issues/3
[harder]: https://github.com/PeculiarVentures/graphene-cli/issues/2
[graphene-cli]: https://github.com/PeculiarVentures/graphene-cli
