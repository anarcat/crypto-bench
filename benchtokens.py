#!/usr/bin/python

'''This script will test decryption performance of GPG, particularly
with smart cards. Note that it will wipe the encryption key of the
smart card you provide it, so it's a dangerous thing to do on a real
card. See the README.md file for more information.'''

from __future__ import print_function

from builtins import range, input

import argparse
import datetime
from distutils.version import LooseVersion
import logging
import os
import psutil
import shutil
import subprocess
import sys
import tempfile

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas
import seaborn as sns


class Timer(object):
    """this class is to track time and resources passed

    originally from bup-cron, but improved to include memory usage"""

    def __init__(self):
        """initialize the timstamp"""
        self.stamp = datetime.datetime.now()

    def times(self):
        """return a string designing resource usage"""
        return 'user %s system %s chlduser %s chldsystem %s' % os.times()[:4]

    def rss(self):
        process = psutil.Process(os.getpid())
        return process.memory_info().rss

    def memory(self):
        return 'RSS %s' % sizeof_fmt_iec(self.rss())

    def diff(self):
        """a datediff between the creation of the object and now"""
        return datetime.datetime.now() - self.stamp

    def __str__(self):
        """return a string representing the time passed and resources used"""
        return 'elasped: %s (%s %s)' % (str(self.diff()),
                                        self.times(),
                                        self.memory())


def sizeof_fmt(num, suffix='B', units=None, power=None,
               sep=' ', precision=2, sign=False):
    """format the given size as a human-readable size"""
    prefix = '+' if sign and num > 0 else ''

    for unit in units[:-1]:
        if abs(round(num, precision)) < power:
            if isinstance(num, int):
                return "{}{}{}{}{}".format(prefix, num, sep, unit, suffix)
            else:
                return "{}{:3.{}f}{}{}{}".format(prefix, num, precision,
                                                 sep, unit, suffix)
        num /= float(power)
    return "{}{:.{}f}{}{}{}".format(prefix, num, precision,
                                    sep, units[-1], suffix)


def sizeof_fmt_iec(num, suffix='B', sep=' ', precision=2, sign=False):
    return sizeof_fmt(num, suffix=suffix, power=1024,
                      units=['', 'Ki', 'Mi', 'Gi', 'Ti',
                             'Pi', 'Ei', 'Zi', 'Yi'],
                      sep=sep, precision=precision, sign=sign)


def load_csv():
    logging.info('loading CSV file %s with pandas', args.datafile)
    data = pandas.read_csv(args.datafile)
    # expected rows format:
    # algorithm,device,time
    # string,string,float

    # convert from time to bandwidth
    if 'bandwidth' not in data:
        logging.info('calculating missing bandwidth')
        data['bandwidth'] = args.size / data['time']
    return data


def render_graph():
    # using standard deviation as it's more commonly understood...
    if LooseVersion(sns.__version__) >= LooseVersion('0.8.0'):
        logging.info('rendering graph with stdev error bars (seaborn %s)',
                     sns.__version__)
        ci = 'sd'
    else:
        # ... we also have concerns seaborn doesn't compute it properly:
        # https://stackoverflow.com/questions/46125182/is-seaborn-confidence-interval-computed-correctly
        logging.info('rendering graph with confidence interval error bars (seaborn %s)',
                     sns.__version__)
        ci = 95
    ax = sns.barplot(data=data, x='algorithm', y=args.variable, hue='device',
                     ci=ci, estimator=np.median,
                     hue_order=args.keycard, palette='colorblind')
    ax.set_title('Decryption performance')
    if args.variable == 'time':
        ax.set_ylabel('time (seconds, logarithmic)')
    elif args.variable == 'bandwidth':
        ax.set_ylabel('bandwidth (byte/second, logarithmic)')
    ax.set_yscale("log")
    ax.get_yaxis().set_major_formatter(mpl.ticker.ScalarFormatter())
    #for p in ax.patches:
    #    ax.annotate(s="%.2f" % p.get_height(),
    #                xy=(p.get_x() + p.get_width() / 2., p.get_height()))
    if args.output == sys.stdout and \
            ('DISPLAY' in os.environ or sys.stdout.isatty()):
        logging.info("drawing on tty")
        plt.show()
    else:
        logging.info('drawing to file %s', args.output)
        plt.savefig(args.output, bbox_inches='tight',
                    format=args.output.name[-3:])


def setup_home(algo, size):
    logging.info('killing gpg-agent so it takes into account our new HOME')
    os.system('killall gpg-agent')
    os.environ['GNUPGHOME'] = tempfile.mkdtemp(prefix='gpgbench-')

    path = os.path.join(os.environ['GNUPGHOME'], 'testfile')
    logging.info('writing %s test file in %s',
                 sizeof_fmt_iec(float(args.size)), path)

    timer = Timer()
    with open(path, 'wb') as testfile:
        testfile.write(os.urandom(args.size))
    logging.debug('wrote file in %s', timer)

    qualifier = 'Length'
    subkey_algo = algo
    subkey_size = size
    if algo.upper().startswith('ED'):
        # ECC hack: change subkey to use encryption-specific algos
        qualifier = 'Curve'
        subkey_algo = 'ECDH'
        subkey_size = 'Curve25519'
    cmd = '''gpg --quiet --expert --batch --generate-key <<EOF
%%transient-key
%%no-protection
Key-Type: %s
Key-%s: %s
Key-Usage: sign
Subkey-Type: %s
Subkey-%s: %s
Subkey-Usage: encrypt
Name-Real: test key
Name-Email: test@example.com
Expire-Date: 1w
EOF''' % (algo, qualifier, size, subkey_algo, qualifier, subkey_size)
    logging.debug('keygen command: %s', cmd)
    logging.info('generating new %s %s key', algo, size)
    if not os.system(cmd) == 0:
        return False
    logging.info('encrypting test file')
    try:
        os.unlink(os.path.join(os.environ['GNUPGHOME'], 'testfile.gpg'))
    except OSError:
        pass
    return os.system('gpg --quiet --encrypt -r test@example.com %s'
                     % os.path.join(os.environ['GNUPGHOME'], 'testfile')) == 0


def delete_home():
    logging.info('removing test gpg dir')
    shutil.rmtree(os.environ['GNUPGHOME'], ignore_errors=True)


def decrypt_file():
    return os.system('gpg --quiet --decrypt < %s > %s'
                     % (os.path.join(os.environ['GNUPGHOME'], 'testfile.gpg'),
                        '/dev/null')) == 0


def decrypt_file_verify():
    return os.system('gpg --quiet --decrypt < %s > %s && cmp %s %s'
                     % (os.path.join(os.environ['GNUPGHOME'], 'testfile.gpg'),
                        os.path.join(os.environ['GNUPGHOME'], 'testfile.test'),
                        os.path.join(os.environ['GNUPGHOME'], 'testfile'),
                        os.path.join(os.environ['GNUPGHOME'], 'testfile.test'))) == 0
                     

def move_to_keycard():
    logging.info('moving private key to keycard')
    proc = subprocess.Popen(['gpg', '--status-fd', '2', '--command-fd', '0',
                             '--batch', '--edit-key', 'test@example.com'],
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)

    class GpgDialogError(Exception):
        pass

    def wait_for_prompt(prompt='[GNUPG:] GET_LINE keyedit.prompt'):
        while True:
            line = proc.stderr.readline().strip()
            logging.debug(line)
            if line == prompt:
                break
            elif line.startswith('[GNUPG:] GET_'):
                raise GpgDialogError('unexpected prompt: %s' % line)

    success = True
    try:
        wait_for_prompt()
        proc.stdin.write("key 1\n")  # select first subkey
        logging.debug(proc.stderr.readline())  # [GNUPG:] GOT_IT
        wait_for_prompt()
        proc.stdin.write("keytocard\n")
        logging.debug("sent 'keytocard'")
        logging.debug(proc.stderr.readline())  # [GNUPG:] GOT_IT
        wait_for_prompt('[GNUPG:] GET_LINE cardedit.genkeys.storekeytype')
        proc.stdin.write("2\n")  # move to encryption slot
        logging.debug("sent 2")
        logging.debug(proc.stderr.readline())  # [GNUPG:] GOT_IT

        # XXX: known bug: this will only happen if the key is not new
        wait_for_prompt('[GNUPG:] GET_BOOL cardedit.genkeys.replace_key')
        proc.stdin.write("y\n")
        wait_for_prompt()
        proc.stdin.write("save\n")
    except GpgDialogError as e:
        logging.warn('failed to communicate with GPG: %s', e)
        success = False

    proc.stderr.close()
    proc.stdout.close()
    proc.stdin.close()
    proc.wait()
    return success and proc.returncode == 0


def gen_test_data():
    logging.info('generating test data')
    data = pandas.DataFrame()
    keycard_curr = 'CPU'
    for keycard in args.keycard:
        for algo_size in args.algorithm:
            (algo, size) = algo_size.split('-', 2)
            if algo.upper().startswith('ED'):
                # ECC hack: store the subkey algo for ECC
                algo_size = 'ECDH-Curve25519'
            if not setup_home(algo, size):
                logging.warning('failed to setup home %s',
                                os.environ['GNUPGHOME'])
                break
            if keycard != 'CPU':
                if keycard != keycard_curr:
                    input("insert keycard %s and press enter to continue: "
                          % keycard)
                    keycard_curr = keycard
                if not move_to_keycard():
                    logging.warning('failed to move to keycard')
                    break
            logging.info('first decryption test to remove password prompt')
            if not decrypt_file_verify():
                logging.warning('failed to decrypt file with keycard %s',
                                keycard)
                break
            logging.info('testing decryption, %d iterations',
                         args.iterations)
            for i in range(args.iterations):
                timer = Timer()
                decrypt_file()
                s = timer.diff().total_seconds()
                data = data.append([(keycard, algo_size, s)])
                print("\rdecryption pass %d [%s]" % (i+1, timer), end='')
                sys.stdout.flush()
            print()
            delete_home()
    data.columns = ['device', 'algorithm', 'time']
    # convert from time to bandwidth
    data['bandwidth'] = args.size / data['time']
    logging.debug('generated dataset: %s', data)
    if args.save:
        logging.info('dataset saved to file %s', args.save)
        data.to_csv(args.save)
    return data


if __name__ == "__main__":
    parser = argparse.ArgumentParser(epilog=__doc__,
                                     description='Benchmark cryptographic tokens')
    parser.add_argument('datafile', default=None, nargs='?',
                        help='CSV file to read instead of running the test)')
    parser.add_argument('--verbose', '-v', dest='log_level',
                        help='show informative messages',
                        action='store_const', const='INFO', default='WARNING')
    parser.add_argument('--debug', '-d', dest='log_level',
                        help='show debug messages',
                        action='store_const', const='DEBUG', default='WARNING')
    parser.add_argument('--output', '-o', type=argparse.FileType('w'),
                        default=sys.stdout, nargs='?', metavar='FILE',
                        help='save graph to FILE or stdout')
    parser.add_argument('--save', type=argparse.FileType('w'), metavar='CSV',
                        help='save generated results to CSV file')
    parser.add_argument('--iterations', '-i', metavar='N',
                        type=int, default=100,
                        help='repeat the test N times (%(default)s)')
    parser.add_argument('--algorithm', '-a', metavar='AGLO', nargs='+',
                        default=['RSA-2048', 'RSA-4096', 'EDDSA-Ed25519'],
                        help='test the given ALGO list (%(default)s)')
    parser.add_argument('--keycard', '-k', nargs='+',
                        default=['CPU'], metavar='NAME',
                        help='test keycards NAME in sequence, also affects which devices are selected to show in the graph (%(default)s)')
    parser.add_argument('--size', '-s', default=16,
                        metavar='SIZE', type=int,
                        help='SIZE of sample file, in bytes (%(default)s)')
    parser.add_argument('--bandwidth', dest='variable', default='time',
                        action='store_const', const='bandwidth',
                        help='plot bandwidth instead of %(default)s')

    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(message)s', level=args.log_level)

    if args.datafile is not None:
        data = load_csv()
    else:
        data = gen_test_data()
    render_graph()
