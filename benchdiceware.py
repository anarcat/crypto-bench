#!/usr/bin/python3

import argparse
import logging
import os
import sys

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

# this script creates a graphic that evaluates the entropy stored in a
# diceware password of a given length. it then computes the
# corresponding time it would take to crack the password by doing a
# bruteforce hash of all possibilities using all the computing power
# of the Bitcoin network, at the time of writing.
#
# it also makes silly jokes along the way.
#
# the parameters are specified below

# max password length, in words, to evaluate
# warning: cranking that beyond may break
maxlen = 25
# hashrate, as a number
# bitcoin: 9,966,505.88 TH/s, or ~10EH/s, or 10*10^18
# source: blockchain.info
hashrate = 10**19
# what is this the hashrate of, for title
what = "Bitcoin network"


def crack(entropy):
    return (2**entropy / hashrate) / (60*60*24*365)  # years


def render_graph(output):
    plt.figure(figsize=(17, 11))
    ax = plt.subplot(211)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(5))
    ax.xaxis.set_minor_locator(ticker.MultipleLocator(1))
    plt.grid(axis='x')
    plt.title('Password entropy')
    plt.xlabel('Password length (words)')
    plt.ylabel('Entropy (bits)')
    # comparison with typical symmetric keys
    plt.axhline(y=128)
    plt.axhline(y=256)
    for line in (128, 256):
        plt.annotate('%d bits symmetric key equivalent'
                     % line, xy=(0, line), xytext=(0, line+2))
    x = np.arange(1, maxlen)
    plt.plot(x, x*np.log2(80000), label='80k word list')
    plt.plot(x, x*np.log2(7776), label='EFF large wordlist (<8k)')
    plt.plot(x, x*np.log2(1296), label='EFF short wordlist (1k)')
    plt.legend()

    ax = plt.subplot(212)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(5))
    ax.xaxis.set_minor_locator(ticker.MultipleLocator(1))
    plt.grid(axis='x')
    plt.title('Time to crack using %gH/s (%s)' % (hashrate, what))  # noqa
    plt.xlabel('Password length (words)')
    plt.ylabel('Time (years, logarithmic)')

    for lines in ((4.0/(365*24), 'time to research this (~4h, not to scale)'),
                  (1000, 'millenia'),
                  (5*10**9, 'age of the universe'),
                  (crack(128), '128 bit symmetric keys'),
                  (crack(256), '256 bit symmetric keys')):
        y, label = lines
        plt.axhline(y=y)
        plt.annotate(label, xy=(0, y), xytext=(0, y))

    x = np.arange(1, maxlen)

    plt.plot(x, crack(x*np.log2(80000)), label='80k word list')
    plt.plot(x, crack(x*np.log2(7776)), label='EFF large wordlist (8k)')
    plt.plot(x, crack(x*np.log2(1296)), label='EFF short wordlist (1k)')
    plt.yscale('log')

    plt.tight_layout()
    if args.output == sys.stdout and \
            ('DISPLAY' in os.environ or sys.stdout.isatty()):
        logging.info("drawing on tty")
        plt.show()
    else:
        logging.info('drawing to file %s', args.output)
        plt.savefig(args.output,
                    format=args.output.name[-3:])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(epilog=__doc__,
                                     description='(Diceware) Password lengths evaluations')  # noqa
    parser.add_argument('--output', '-o', type=argparse.FileType('w'),
                        default=sys.stdout, nargs='?', metavar='FILE',
                        help='save graph to FILE or stdout')
    logging.basicConfig(format='%(asctime)s %(message)s')
    args = parser.parse_args()
    render_graph(args.output)
